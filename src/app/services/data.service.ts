import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of, Observable, EMPTY, NEVER, Subject, Observer, ReplaySubject } from 'rxjs';
import { tap, delayWhen, map } from 'rxjs/operators';

import { environment } from '../../environments/environment';

import { workersObjects } from "@objects/workers/models/workers.model";
import { professionsObjects } from "@objects/professions/models/professions.model";
import { dutiesObjects } from "@objects/duties/models/duties.model";
import { dbRef } from "@objects/models/object.model";

import { extractDate } from "@shared/other.util";

interface Idb {
  workers:                  workersObjects.IRef[];
  professions:              professionsObjects.IRef[];
  duties:                   dutiesObjects.IRef[];
  workersTableProfessions:  workersObjects.ITableProfessions[];
  professionsTableDuties:   professionsObjects.ITableDuties[];
}//

interface ISearch {
  id:dbRef;
  text: string;
}//

export enum EVariantSearch {
  all = "All",
  name = "Name",
  profession = "Profession",
  duty = "Duty"
}//


@Injectable({ providedIn: 'root' })
export class DataService {

  countPers = 100;
  workersUrl = "https://randomuser.me/api/?results=" + this.countPers;
  professionsUrl = `${environment.localDbUrl}professions.json`;
  dutiesUrl = `${environment.localDbUrl}duties.json`;


  private _db:Idb = {
    workers: [],
    professions: [],
    duties: [],
    workersTableProfessions: [],
    professionsTableDuties: []
  }

  private _getWorkers(db?:workersObjects.IRef[]):workersObjects.IRef[] { 
    if (db) this._db.workers = db;
    return this._db.workers;
  }// 
  private _getProfessions(db?:professionsObjects.IRef[]): professionsObjects.IRef[] {
    if (db) this._db.professions = db;
    return this._db.professions;
  }//
  private _getDuties(db?:dutiesObjects.IRef[]): dutiesObjects.IRef[] {
    if (db) this._db.duties = db;
    return this._db.duties;
  }//
  private _getWorkersTableProfessions(db?:workersObjects.ITableProfessions[]): workersObjects.ITableProfessions[] {
    if (db) this._db.workersTableProfessions = db;
    return this._db.workersTableProfessions;
  }//
  private _getProfessionsTableDuties(db?:professionsObjects.ITableDuties[]): professionsObjects.ITableDuties[] {
    if (db) this._db.professionsTableDuties = db;
    return this._db.professionsTableDuties;
  }//


  private _onReady$ = new ReplaySubject<any>(1);

  //////////////////////////////////////////////  

  
  constructor(private _http: HttpClient) { 
    this.initialiseData();
  }//

  sortWorkers(m: workersObjects.IRef[]) {
    m.sort((a,b)=>{
      if (a.lastName > b.lastName) return 1;
      if (a.lastName < b.lastName) return -1;
      if (a.lastName === b.lastName) {
        if (a.firstName > b.firstName) return 1;
        if (a.firstName < b.firstName) return -1;
      }
      return 0;
    }); 
  }//

  initWorkers(list: any[]) {

    list = list.filter(el=>{
      return el.gender === "female" && el.registered.age < 30;
    });

    let count = 0;
    let m = [];
    this._getWorkers().length=0;

    list.forEach(el => {
      count++;
      const rec: workersObjects.IRef = {
        id: 'workers=' + el.login.uuid,
        code: ('000000' + count).slice(-6),
        //
        firstName: el.name.first,
        lastName: el.name.last,
        dateOfBirth: extractDate(el.dob.date),
        fotoLargeUrl: el.picture.large,
        fotoMediumUrl: el.picture.medium,
        fotoThumbnailUrl: el.picture.thumbnail
      };
      m.push(rec);
    });

    const regexp = /^[A-Za-z]+$/i;
    m = m.filter(el=>{return regexp.test(el.lastName)});
    this.sortWorkers(m);
    m.forEach(el=>{ this._getWorkers().push(el) });

  }//

  initLinkedTabs() {

    //Каждому профессию
    const lengthProfessions = this._getProfessions().length;
    if (!lengthProfessions) return;
    let i = 0;

    for (let el of this._getWorkers()) {
      let row: workersObjects.ITableProfessions = { 
        worker: el.id,
        rowNumber: 0,
        profession: this._getProfessions()[i].id
      };
      this._getWorkersTableProfessions().push(row);
      i++;
      if (i===lengthProfessions) i=0;
    };

    //В каждой профессии - обязанность
    for (let i=0; i < 3; i++) {
      this._getProfessionsTableDuties().push({
        profession: this._getProfessions()[i].id,
        rowNumber: 0,
        duty: this._getDuties()[i].id
      });
    };

    // console.log('tables Workers', this._getWorkers());
    // console.log('tables Professions', this._getProfessions());
    // console.log('tables Duties', this._getDuties());
    // console.log('tables WorkersTableProfessions', this._getWorkersTableProfessions());
    // console.log('tables ProfessionsTableDuties', this._getProfessionsTableDuties());

  }//

  initialiseData() {
    
    let workes$ = this._http.get<Record<string, any>>(this.workersUrl)
    .pipe(
      tap(val=>{ this.initWorkers(val.results) })
    );

    let profrssions$ = this._http.get<professionsObjects.IDataHTTP>(this.professionsUrl)
    .pipe(
      tap(val=>{ this._getProfessions(val.list) })
    );

    let duties$ = this._http.get<dutiesObjects.IDataHTTP>(this.dutiesUrl)
    .pipe(
      tap(val=>{ this._getDuties(val.list) })
    );

    of(0).pipe(
      delayWhen(_=>workes$),
      delayWhen(_=>profrssions$),
      delayWhen(_=>duties$)
    ).subscribe(_=>{
      this.initLinkedTabs();
      this._onReady$.next(0);
      this._onReady$.complete();
    });

  }//
  
  ///////////////////////////////////////////////


  private getList<T>(base: ()=>T[] ): ()=>Observable<T[]> {
    const ready$ = this._onReady$;
    return ()=>{
      return of(0).pipe(
        delayWhen(_=>ready$),
        map(_=>base.call(this))
        );
    };
  }//

  //public getWorkersList     = this.getList<workersObjects.IRef>(this._getWorkers);
  public getProfessionsList = this.getList<professionsObjects.IRef>(this._getProfessions);
  public getDutiesList      = this.getList<dutiesObjects.IRef>(this._getDuties);

  /////////////////////////////////////////////////////////////////////////////////////////////////////////

  // private search_v1(filter: string[], base: ISearch[]): dbRef[] {

  //   const rezult = base.filter(el=>{
  //     let fl = true;
  //     filter.forEach(str=>{ fl = fl && el.text.includes(str) });
  //     return fl;
  //   });

  //   return rezult.map(el=>el.id);
  // }//

 

  public getWorkersList( variant: EVariantSearch, filterStr: string ): Observable<workersObjects.IRef[]> {
    
    const ready$ = this._onReady$;
    let filter =  (!filterStr) ? [] : filterStr.split(" ");
    filter = filter.map(el=>el.toLowerCase()).filter(el=>el);

    return of(0).pipe(
      delayWhen(_=>ready$),
      map(_=>{ return this.filteredWorkers(variant, filter) })
    );
    
  }//
  
  // private filteredWorkers_v1(variant: EVariantSearch, filter: string[]):workersObjects.IRef[] {

  //   filter = filter.map(el=>el.toLowerCase()).filter(el=>el);

  //   if (!filter.length) return this._getWorkers();

  //   let base: ISearch[];
  //   let list = [];

  //   //по списку работников
  //   if (variant === EVariantSearch.all || variant === EVariantSearch.name) {
  //     base = this._getWorkers().map(el=>{
  //       return {
  //         id: el.id,
  //         text: (el.firstName + " " + el.lastName).toLowerCase()
  //       };
  //     });

  //     list = list.concat(this.search(filter, base));
  //   }

  //   //по списку профессий
  //   if (variant === EVariantSearch.all || variant === EVariantSearch.profession) {
  //     base = this._getProfessions().map(el=>{
  //       return {
  //         id: el.id,
  //         text: el.name.toLowerCase()
  //       };
  //     });
  //     const masProf = this.search(filter, base);
  //     list = list.concat( this._getWorkersTableProfessions().filter(el=>{ return masProf.includes(el.profession) }).map(el=>el.worker) );
  //   }
    
  //   //по списку обязанностей    
  //   if (variant === EVariantSearch.all || variant === EVariantSearch.duty) {
  //     base = this._getDuties().map(el=>{
  //       return {
  //         id: el.id,
  //         text: el.name.toLowerCase()
  //       };
  //     });
  //     let masDuties = this.search(filter, base);
  //     let masProfessionsByDuties = this._getProfessionsTableDuties().filter(el=>{ return masDuties.includes(el.duty) }).map(el=>el.profession);
  //     list = list.concat( this._getWorkersTableProfessions().filter(el=>{ return masProfessionsByDuties.includes(el.profession) }).map(el=>el.worker) );
  //   }

  //   //итог
  //   return this._getWorkers().filter(el=>{ return list.includes(el.id) });
    
  // }//


  private search(filter: string[], base: ISearch[]): dbRef[] {

    const rezult = base.filter(el=>{
      let fl = true;
      filter.forEach(str=>{ fl = fl && el.text.includes(str) });
      return fl;
    });

    return rezult.map(el=>el.id);
  }//

  private concatBase(base: ISearch[], part: ISearch[]) {

    for (const el of part) {
      const item = base.find(val=>{return val.id === el.id});
      if (item) {
        item.text = item.text + " " + el.text;
      } else {
        base.push(el);
      }
    };
  }//


  private filteredWorkers(variant: EVariantSearch, filter: string[]):workersObjects.IRef[] {

    filter = filter.map(el=>el.toLowerCase()).filter(el=>el);

    if (!filter.length) return this._getWorkers();

    let base: ISearch[] = [];

    //по списку работников
    if (variant === EVariantSearch.all || variant === EVariantSearch.name) {
      const tabByName = this._getWorkers().map(el=>{
        return {
          id: el.id,
          text: (el.firstName + " " + el.lastName).toLowerCase()
        };
      });
      this.concatBase(base, tabByName);
    }

    //по списку профессий
    if (variant === EVariantSearch.all || variant === EVariantSearch.profession) {
      const tabProfession = this._getProfessions().map(el=>{
        return {
          id: el.id,
          text: el.name.toLowerCase()
        };
      });

      const tabByProfession = this._getWorkersTableProfessions().map(el=>{
        const item = tabProfession.find(val=>{return val.id===el.profession});
        if (item) {
          return {id: el.worker, text: item.text};
        } else {
          return {id: "", text: ""}
        }
      });

      this.concatBase(base, tabByProfession);
    }
    
    //по списку обязанностей    
    if (variant === EVariantSearch.all || variant === EVariantSearch.duty) {

      const tabDuties = this._getDuties().map(el=>{
        return {
          duty: el.id,
          text: el.name.toLowerCase()
        };
      });

      const tabDutiesInProfessions = [];
      this._getProfessionsTableDuties().forEach(el=>{
        const item = tabDuties.find(eld=>{return eld.duty===el.duty});
        if (item) { tabDutiesInProfessions.push( {profession: el.profession, text: item.text} )}
      });

      const tabByDuties: ISearch[] = [];
      this._getWorkersTableProfessions().forEach(elw=>{
        const item = tabDutiesInProfessions.find(val=>{return val.profession===elw.profession});
        if (item) { tabByDuties.push( {id:elw.worker, text: item.text} )}
      });

      this.concatBase(base, tabByDuties);
    }

    const mas = this.search(filter, base);

    //итог
    return this._getWorkers().filter(el=>{ return mas.includes(el.id) });
    
  }//



//////////////////////////////////////////////////////////////////////////////////////////////////


  private getItem<T extends {id:dbRef} >(base: ()=>T[]): (id:dbRef)=>Observable<T> {
    const ready$ = this._onReady$;
    return (id:dbRef)=>{
      return of(0).pipe(
        delayWhen(_=>ready$),
        map(_=>{
          const item = base.call(this).find(el=>el.id === id);
          return (item) ? item : undefined; 
        })
      );
    };
  }//

  public getWorkersItem     = this.getItem<workersObjects.IRef>(this._getWorkers);
  public getProfessionsItem = this.getItem<professionsObjects.IRef>(this._getProfessions);
  public getDutiesItem      = this.getItem<dutiesObjects.IRef>(this._getDuties);

  //

  private getRecordset<T>(base: ()=>T[]): (field:string, id:dbRef)=>Observable<T[]> {
    const ready$ = this._onReady$;
    return (field:string, id:dbRef)=>{
      return of(0).pipe(
        delayWhen(_=>ready$),
        map(_=>base.call(this).filter(el=>el[field]===id))
      );
    };
  }//

  public getProfessionsForWorkers = this.getRecordset<workersObjects.ITableProfessions>(this._getWorkersTableProfessions);
  public getDutiesForProfessions  = this.getRecordset<professionsObjects.ITableDuties>(this._getProfessionsTableDuties);

  //

  public getTableBrotherOfMind(pers:dbRef):Observable<workersObjects.IRef[]>{
    
    return of(0).pipe(
      delayWhen(_=>this._onReady$),
      map(_=>{
        //список профессий работника
        const masProfessions = this._getWorkersTableProfessions().filter(el=>{return el.worker===pers && el.profession}).map(el=>el.profession);
        if (!masProfessions.length) return [];

        //список обязанностей по его профессиям
        const masDuties = this._getProfessionsTableDuties().filter(el=>{return masProfessions.includes(el.profession) && el.duty}).map(el=>el.duty);
        if (!masDuties.length) return [];

        //список всех профессий по этим обязанностям
        const masAllProfessions = this._getProfessionsTableDuties().filter(el=>{return masDuties.includes(el.duty) && el.profession}).map(el=>el.profession);
        if (!masAllProfessions.length) return [];

        //список коллег
        const masBrothers = this._getWorkersTableProfessions().filter(el=>{
          return masAllProfessions.includes(el.profession) && el.worker && el.worker !== pers
        }).map(el=>el.worker);

        //Возвращаем "полные" данные
        return this._getWorkers().filter(el=>{return masBrothers.includes(el.id)});
      })
    );
  }//

  //
  public getTableProfessionsForWorkers(pers:dbRef):Observable<professionsObjects.IRef[]>{
    
    return of(0).pipe(
      delayWhen(_=>this._onReady$),
      map(_=>{
        //список профессий работника
        const masProfessions = this._getWorkersTableProfessions().filter(el=>{return el.worker===pers && el.profession}).map(el=>el.profession);
        if (!masProfessions.length) return [];

        //Возвращаем "полные" данные
        return this._getProfessions().filter(el=>{return masProfessions.includes(el.id)});
      })
    );
  }//

  //
  public getTableDutiesForProfessions(prof:dbRef):Observable<dutiesObjects.IRef[]>{
    
    return of(0).pipe(
      delayWhen(_=>this._onReady$),
      map(_=>{
        //список обязанностей в профессии
        const masDuties = this._getProfessionsTableDuties().filter(el=>{return el.profession===prof && el.duty}).map(el=>el.duty);
        if (!masDuties.length) return [];

        //Возвращаем "полные" данные
        return this._getDuties().filter(el=>{return masDuties.includes(el.id)});
      })
    );
  }//


  //

  private saveItem<T extends {id:dbRef} >(base: (db?:T[])=>T[], postWare?:()=>void): (rec:T)=>Observable<T> {
    const ready$ = this._onReady$;
    return (rec:T)=>{
      return of(0).pipe(
        delayWhen(_=>ready$),
        map(_=>{
          base.call(this, base.call(this).filter(el=>el.id!==rec.id));
          base.call(this).push(rec);
          return rec; 
        }),
        tap(_=>{if (postWare) postWare()})
      );
    };
  }//

  public saveWorkersItem     = this.saveItem<workersObjects.IRef>(this._getWorkers, ()=>{this.sortWorkers(this._getWorkers())});
  public saveProfessionsItem = this.saveItem<professionsObjects.IRef>(this._getProfessions);
  public saveDutiesItem      = this.saveItem<dutiesObjects.IRef>(this._getDuties);

  //

  public saveTableDutiesForProfessions(profession:dbRef, obj: professionsObjects.ITableDuties[]):Observable<any> {
    const ready$ = this._onReady$;
    const base = this._getProfessionsTableDuties;

    return of(0).pipe(
      delayWhen(_=>ready$),
      tap(_=>{
        const t = base.call(this).filter(el=>{return el.profession !== profession});
        obj.forEach(el=>{ t.push(el) });
        base.call(this, t);
      })
    );
  }//

  //

  public saveTableProfessionsForWorkers(worker:dbRef, obj: workersObjects.ITableProfessions[]):Observable<any> {
    const ready$ = this._onReady$;
    const base = this._getWorkersTableProfessions;

    return of(0).pipe(
      delayWhen(_=>ready$),
      tap(_=>{
        const t = base.call(this).filter(el=>{return el.worker !== worker});
        obj.forEach(el=>{ t.push(el) });
        base.call(this, t);
      })
    );
  }//

  //

  private deleteItem<T extends {id:dbRef} >(base: (db?:T[])=>T[]): (id:dbRef)=>Observable<any> {
    const ready$ = this._onReady$;
    return (id:dbRef)=>{
      return of(0).pipe(
        delayWhen(_=>ready$),
        map(_=>{
          base.call(this, base.call(this).filter(el=>el.id!==id));
          return 0; 
        })
      );
    };
  }//

  public deleteWorkersItem     = this.deleteItem<workersObjects.IRef>(this._getWorkers);
  public deleteProfessionsItem = this.deleteItem<professionsObjects.IRef>(this._getProfessions);
  public deleteDutiesItem      = this.deleteItem<dutiesObjects.IRef>(this._getDuties);


}//