import { Component, OnInit, Injector } from '@angular/core';

import { dutiesObjects } from "../models/duties.model";
import { DutiesListState } from '../store/duties.state';
import { actionsDuties as Actions } from "../store/duties.actions";

import { Observable } from 'rxjs';
import { ListContainer } from "@shared/list.container";
import { delayWhen } from 'rxjs/operators';
import { ModalController } from '@ionic/angular';

@Component({
  templateUrl: './select-duties.container.html',
  styleUrls: ['./select-duties.container.scss']
})
export class SelectDutiesContainer extends ListContainer implements OnInit {

  public object$: Observable<dutiesObjects.List>;

  constructor( protected readonly injector: Injector,
    public modalController: ModalController
    ){
    super(injector);

    this.environs = {
      State : DutiesListState,
      actions: { 
        GetList: Actions.GetList
      },
      model: dutiesObjects,
      selectors: { GetList: DutiesListState.selectorGetList }
    };

  }//

  ngOnInit() {
    this.loadDate();
  }

  selectRow(row) {
    this.modalController.dismiss({'duty': row });
  }//

  onClose() {
    this.modalController.dismiss();
  }//
    

}//
