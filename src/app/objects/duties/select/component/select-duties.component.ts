import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { dutiesObjects } from "../../models/duties.model";
import { ListComponent } from "@shared/list.component";

@Component({
  selector: 'app-select-duties',
  templateUrl: './select-duties.component.html',
  styleUrls: ['./select-duties.component.scss']
})
export class SelectDutiesComponent extends ListComponent {

  @Input() list: dutiesObjects.Ref[];
  @Output() selectRow = new EventEmitter<dutiesObjects.Ref>();

  constructor() {
    super();
  }//

  selectItem(row) {
    this.selectRow.emit(row);
  }//

}//
