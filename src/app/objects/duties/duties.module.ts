import { NgModule } from '@angular/core';
import { SharedModule } from "@app/modules/shared.module";
//
import { ListDutiesContainer } from "./list/list-duties.container";
import { ListDutiesComponent } from "./list/component/list-duties.component";
import { RefDutiesContainer } from "./ref/ref-duties.container";
import { RefDutiesComponent } from "./ref/component/ref-duties.component";
import { DutiesRoutingModule } from "./duties.routing";
import { SelectDutiesContainer } from "./select/select-duties.container";
import { SelectDutiesComponent } from "./select/component/select-duties.component";

@NgModule({

  imports: [
    SharedModule,
    DutiesRoutingModule,
  ],

  declarations: [
    ListDutiesContainer,
    ListDutiesComponent,
    RefDutiesContainer,
    RefDutiesComponent,

    SelectDutiesContainer,
    SelectDutiesComponent

  ],

  exports: [
    DutiesRoutingModule,
    //
    ListDutiesContainer,
    ListDutiesComponent,
    RefDutiesContainer, 
    RefDutiesComponent,

    SelectDutiesContainer

  ],

  entryComponents: [
    SelectDutiesContainer,
    SelectDutiesComponent
  ]

})
export class DutiesModule {}
