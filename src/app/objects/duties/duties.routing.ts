import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListDutiesContainer } from "./list/list-duties.container";
import { RefDutiesContainer } from "./ref/ref-duties.container";
import { dutiesObjects } from "./models/duties.model";


const routes: Routes = [
  
  { path: 'list-duties',//workersObjects.urlList,  
    pathMatch: 'full', 
    component: ListDutiesContainer 
  },
  { path: 'ref-duties/:id',//workersObjects.urlRef,  
    pathMatch: 'full', 
    component: RefDutiesContainer 
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class DutiesRoutingModule { }
