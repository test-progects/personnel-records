
import * as objectModel from "@objects/models/object.model";
import { getNewReference } from "@shared/dbRef.util";
import { fillProp } from "@shared/other.util";
import { Observable, of, EMPTY, concat } from 'rxjs';
import { toArray } from 'rxjs/operators';
import { DataService } from "@services/data.service";


export namespace dutiesObjects {

    export interface IRef {
        id:         objectModel.dbRef;
        name:       string;
    }//

    export interface IDataHTTP {
        list:       IRef[];
    }//

    export const prefixDbRef    = 'duties';
    export const urlRef         = 'ref-duties';
    export const urlList        = 'list-duties';

    export class Ref extends objectModel.MetadataObject implements IRef  {

        id:   objectModel.dbRef = null;
        name: string            = null;
        //
        share: Record<string, any> = {};
        newItem = false;

        constructor ( record?: IRef ) {

            super();
            if (!record) {
                //Это новый объект
                this.newItem = true; 
                this.id = getNewReference(prefixDbRef);
                return;
            };

            fillProp(this, record);
        }//

        refresh(dataServise: DataService): Observable<any> {
            return of(0);
        }//

        getRecordDB () {
            return {
                id:             this.id,
                name:           this.name,
            }
        }//
        
    }//

    export class List {
    
        public list: Ref[] = [];

        constructor ( recordList?: IRef[] ) {
            if (!recordList) return;
            for (let rec of recordList) { this.list.push( new Ref(rec) ) }
        }//

        refresh(dataServise: DataService): Observable<any> {

            let o$: Observable<any> = EMPTY;
            this.list.forEach(el => { 
                o$ = concat(o$, el.refresh(dataServise));
            });
            return o$.pipe(toArray());
        }//

    }//



}//