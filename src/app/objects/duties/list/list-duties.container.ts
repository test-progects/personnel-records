import { Component, OnInit, Injector } from '@angular/core';

import { dutiesObjects } from "../models/duties.model";
import { DutiesListState } from '../store/duties.state';
import { actionsDuties as Actions } from "../store/duties.actions";

import { Observable } from 'rxjs';
import { ListContainer } from "@shared/list.container";

@Component({
  templateUrl: './list-duties.container.html',
  styleUrls: ['./list-duties.container.scss']
})
export class ListDutiesContainer extends ListContainer {

  public object$: Observable<dutiesObjects.List>;

  constructor( protected readonly injector: Injector){
    super(injector);

    this.environs = {
      State : DutiesListState,
      actions: { 
        GetList: Actions.GetList
      },
      model: dutiesObjects,
      selectors: { GetList: DutiesListState.selectorGetList }
    };
  
  }//

  onNew() {
    this.router.navigate(['/' + dutiesObjects.urlRef, ""]);
  }//

  openCustomMenu() {
    this.customMenu("list-duties-menu");
  }//
}//
