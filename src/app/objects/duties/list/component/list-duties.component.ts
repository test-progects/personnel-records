import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { dutiesObjects } from "../../models/duties.model";
import { ListComponent } from "@shared/list.component";

@Component({
  selector: 'app-list-duties',
  templateUrl: './list-duties.component.html',
  styleUrls: ['./list-duties.component.scss']
})
export class ListDutiesComponent extends ListComponent {

  @Input() list: dutiesObjects.Ref[];
  @Output() selectRow = new EventEmitter<dutiesObjects.Ref>();

  constructor() {
    super();
  }//

}//
