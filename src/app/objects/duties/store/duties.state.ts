import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector, createSelector } from '@ngxs/store';
import { tap } from 'rxjs/operators';

import { dbRef } from "@objects/models/object.model";
import { dutiesObjects } from "../models/duties.model";
import { DataService } from "@services/data.service";
import { actionsDuties as Actions} from "./duties.actions";


interface IModelListState {
  list: dutiesObjects.IRef[];
}//

@State<IModelListState>({
  name: 'dutiesList',
  defaults: {
    list: []
  }
})
@Injectable()
export class DutiesListState {

  constructor( private _dataService: DataService ) { }

  @Action(Actions.GetList)
  actionsGetList(ctx: StateContext<IModelListState>) {
    return this._dataService.getDutiesList().pipe(
      tap( (list) => { 
        ctx.patchState( {list: list} );
      })
    );
  }//

  @Selector()
  static selectorGetList(state: IModelListState) {
    return state.list;
  }//

}//


interface IModelItemState {
  item: dutiesObjects.IRef;
}//

@State<IModelItemState>({
  name: 'dutiesItem',
  defaults: {
    item: undefined
  }
})
@Injectable()
export class DutiesItemState {

  constructor( private _dataService: DataService ) { }

  @Action(Actions.GetItem)
  actionsGetItem(ctx: StateContext<IModelItemState>, {id}: Actions.GetItem) {
    if (!id) return undefined;
    return this._dataService.getDutiesItem(id).subscribe(
      (val)=>{ 
        if (val) ctx.patchState( {item: val} );
      }
    );
  }//

  static selectorGetItem(id:dbRef) {
    return createSelector([DutiesItemState], (state: IModelItemState) => {
      //return (state.item.id === id) ? new dutiesObjects.Ref(state.item) : undefined;
      return (state.item.id === id) ? state.item : undefined;
      });
  }//

  @Action(Actions.NewItem)
  actionsNewItem(ctx: StateContext<IModelItemState>) {
  }//

  @Action(Actions.SaveItem)
  actionsSaveItem(ctx: StateContext<IModelItemState>, {obj}: Actions.SaveItem) {
    return this._dataService.saveDutiesItem(obj);
  }//

  @Action(Actions.DeleteItem)
  actionsDeleteItem(ctx: StateContext<IModelItemState>, {id}: Actions.DeleteItem) {
    if (!id) return undefined;
    return this._dataService.deleteDutiesItem(id);
  }//


}//

