import { dbRef } from "@objects/models/object.model";
import { dutiesObjects } from "../models/duties.model";


export namespace actionsDuties {

    export class GetList {
        static readonly type = '[Duties] GetList';
        constructor() {}
    }//

    export class GetItem {
        static readonly type = '[Duties] GetItem';
        constructor(public id:dbRef) {}
    }//

    export class NewItem {
        static readonly type = '[Duties] NewItem';
        constructor() {}
    }//

    export class SaveItem {
        static readonly type = '[Duties] SaveItem';
        constructor(public obj:dutiesObjects.IRef) {}
    }//

    //
    export class DeleteItem {
        static readonly type = '[Duties] DeleteItem';
        constructor(public id:dbRef) {}
    }//

}//

