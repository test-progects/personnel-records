import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, Injector } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { dutiesObjects } from "../../models/duties.model";
import { RefComponent } from "@shared/ref.component";

@Component({
  selector: 'app-ref-duties',
  templateUrl: './ref-duties.component.html',
  styleUrls: ['./ref-duties.component.scss']
})
export class RefDutiesComponent extends RefComponent {

  @Input() object: dutiesObjects.Ref;

  form: FormGroup;

  formOptions = { 
    formModified: false
  }

  constructor( 
    protected readonly injector: Injector,
    protected readonly formBuilder: FormBuilder 
    ) {

    super(injector);

    this.form = formBuilder.group({
      name:          ['', [Validators.required]]
    });
  }//

  formToObject() {

    super.formToObject();

    const v = this.form.value;
    this.object.name    = v.name;
  }//

  objectToForm() {
    const data = {
      name:         this.object.name
    };
    this.form.patchValue(data);
  }//
 
  ngOnInit() {   
  }//

  openCustomMenu() {
    this.customMenu("ref-duties-menu");
  }//


}//
