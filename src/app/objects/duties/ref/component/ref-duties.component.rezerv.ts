import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { dutiesObjects } from "../../models/duties.model";
import { MenuController } from '@ionic/angular';

import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-ref-duties',
  templateUrl: './ref-duties.component.html',
  styleUrls: ['./ref-duties.component.scss']
})
export class RefDutiesComponent implements OnInit, OnChanges {

  @Input() object: dutiesObjects.Ref;

  @Output() eventForm = new EventEmitter<any>();

  form: FormGroup;

  formOptions = { 
    formModified: false
  }

  constructor( 
    private formBuilder: FormBuilder,
    private menu: MenuController,
    private alertControl: AlertController
    ) {

    this.form = formBuilder.group({
      name:          ['', [Validators.required]]
    });
  }//

  ngOnChanges(change: SimpleChanges) {
    this.objectToForm();
    this.formOptions.formModified = false;
  }//

  marckModified(){return (this.formOptions.formModified) ? "*" : ""}

  formToObject() {
    this.formOptions.formModified = true;
    
    const v = this.form.value;
    this.object.name    = v.name;
  }//

  objectToForm() {
    const data = {
      name:         this.object.name
    };
    this.form.patchValue(data);
  }//

 
  ngOnInit() {   
  }//

  emitEventForm(name:string, value?, event?) {
    this.eventForm.emit({ name: name, value: value, event: event });
  }//

  onChangeField(event, name) {
    this.formToObject();
    this.emitEventForm(name, this.form.value[name], event);
  }//


  openCustomMenu() {
    //this.menu.enable(true, "custom-menu");
    this.menu.open("custom-menu");
  }//

  onSave() {
    this.getConfirm('Save item?', "",()=>{this.saveThisItem()});
  }//

  saveThisItem() {
    this.formToObject();
    this.formOptions.formModified = false;
    this.emitEventForm("onSave", true);
  }//

  onDelete() {
    this.getConfirm('Delete item?', "",()=>{this.deleteThisItem()});
  }//

  deleteThisItem() {
    this.formToObject();
    this.emitEventForm("onDelete", true);
  }//
  
  async getConfirm(header:string="", message:string="", callback:()=>void) {

    const alert = await this.alertControl.create({
      header: header,
      message: message,
      buttons: [{text:'Yes', handler:callback}, 'No']
    });

    await alert.present();
  }//




}//
