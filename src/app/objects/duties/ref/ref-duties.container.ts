import { Component, OnInit, Injector } from '@angular/core';
import { Observable } from 'rxjs';
//
import { dutiesObjects } from "../models/duties.model";
import { DutiesItemState } from '../store/duties.state';
import { actionsDuties as Actions } from "../store/duties.actions";

import { RefContainer } from "@shared/ref.container";


@Component({
  templateUrl: './ref-duties.container.html',
  styleUrls: ['./ref-duties.container.scss']
})
export class RefDutiesContainer extends RefContainer implements OnInit {

  public object$: Observable<dutiesObjects.Ref>;
  public object: dutiesObjects.Ref;

  constructor(protected readonly injector: Injector) {
    super(injector);

    this.environs = {
      State : DutiesItemState,
      actions: { 
        GetItem: Actions.GetItem,
        DeleteItem: Actions.DeleteItem,
        NewItem: Actions.NewItem,
        SaveItem: Actions.SaveItem
      },
      model: dutiesObjects,
      selectors: { GetItem: DutiesItemState.selectorGetItem }
    };

  }//

  ngOnInit() {}


}//
