import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { delayWhen } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
//
import { dutiesObjects } from "../models/duties.model";
import { Store } from '@ngxs/store';
import { DutiesItemState as State } from '../store/duties.state';
import { actionsDuties as Actions } from "../store/duties.actions";
import { DataService } from "@services/data.service";
import { dbRef } from "@objects/models/object.model";


@Component({
  templateUrl: './ref-duties.container.html',
  styleUrls: ['./ref-duties.container.scss']
})
export class RefDutiesContainer implements OnInit {

  public object$: Observable<dutiesObjects.Ref>;
  public object: dutiesObjects.Ref;
  private _id: string;

  constructor( 
    private store: Store, 
    private _dataService: DataService,
    private route: ActivatedRoute,
    private router: Router,
    ) {
      route.params.subscribe((par) => {
        this._id = par['id'];
        this.loadData();
      });
    }//

  ngOnInit() {}

  loadData() {
    const id = this._id as dbRef;
    this.store.dispatch(new Actions.GetItem(id)).toPromise().then(_=>{
      
      this.store.selectOnce(State.selectorGetItem(id))
      .pipe(
        delayWhen(val=>{return val.refresh(this._dataService)})
      ).toPromise().then(val=>{ 
        this.object = val;
      });
    });

    this.object = new dutiesObjects.Ref();
  }//

  eventForm(ctx) {

    switch (ctx.name) {
      case "onSave": this.onSave();
        break;

      case "onDelete": this.onDelete();
        break;
    
      default:
        break;
    }
    
  }//
  
  onSave() {
    const record = this.object.getRecordDB();
    this.store.dispatch(new Actions.SaveItem(record));
    this.router.navigate(['/' + dutiesObjects.urlList]);
  }//

  onDelete() {
    this.store.dispatch(new Actions.DeleteItem(this.object.id));
    this.router.navigate(['/' + dutiesObjects.urlList]);
  }//

}//
