
import * as objectModel from "@objects/models/object.model";
import { getNewReference } from "@shared/dbRef.util";
import { fillProp } from "@shared/other.util";
import { Observable, of, EMPTY, concat } from 'rxjs';
import { toArray, mergeMap, delayWhen, tap, map } from 'rxjs/operators';
import { DataService } from "@services/data.service";
import { professionsObjects } from "@objects/professions/models/professions.model";


export namespace workersObjects {

    export interface IRef {
        id:                 objectModel.dbRef;
        code:               string;
        //
        firstName:          string;
        lastName:           string;
        dateOfBirth:        Date | string;
        fotoLargeUrl:       string;
        fotoMediumUrl:      string;
        fotoThumbnailUrl:   string
    }//

    export interface ITableProfessions {
        worker:     objectModel.dbRef;
        rowNumber:  number;
        profession: objectModel.dbRef;
    }//

    export const prefixDbRef    = 'workers';
    export const urlRef         = 'ref-workers';
    export const urlList        = 'list-workers';

    export class Ref extends objectModel.MetadataObject implements IRef  {

        id:                 objectModel.dbRef           = null;
        code:               string                      = null;
        //
        firstName:          string                      = null;
        lastName:           string                      = null;
        dateOfBirth:        Date                        = null;
        fotoLargeUrl:       string                      = null;
        fotoMediumUrl:      string                      = null;
        fotoThumbnailUrl:   string                      = null;
        //
        tableProfessions:   professionsObjects.Ref[]    = [];
        tableColleagues:    Ref[]                       = [];
        //
        share: Record<string, any> = {};
        newItem = false;
        

        constructor ( record?: IRef ) {

            super();
            if (!record) {
                //Это новый объект
                this.newItem = true;                
                this.id = getNewReference(prefixDbRef);
                return;
            };

            fillProp(this, record);
        }//

        getColleagues(dataServise: DataService): Observable<List> {
            return dataServise.getTableBrotherOfMind(this.id).pipe(
                map(coll=>{ return new workersObjects.List(coll) }),
                delayWhen(list=>list.refresh(dataServise))
            );
        }//

        getProfessionsTitle() {
            let s = "";
            if (this.tableProfessions.length > 0) s=this.tableProfessions[0].name;
            if (this.tableProfessions.length > 1) s=s + ", " + this.tableProfessions[1].name;
            if (!s) s = "...";
            return s;
        }//

        refresh(dataServise: DataService): Observable<any> {
            
            if (!this.id || this.newItem) return of(0);

            //professions
            let prof$ = dataServise.getTableProfessionsForWorkers(this.id).pipe(
                map(prof=>{ return new professionsObjects.List(prof) }),
                delayWhen(list=>list.refresh(dataServise)),
                tap(list=>{ 
                    this.tableProfessions = list.list;
                    //
                })
            );

            //+ еще операции

            let o$: Observable<any> = EMPTY;
            o$ = concat(o$, prof$);
            return o$.pipe(toArray());
        }//

        getRecordDB () {
            return {
                id:             this.id,
                code:           this.code,
                //
                firstName:      this.firstName,
                lastName:       this.lastName,
                dateOfBirth:    this.dateOfBirth,
                fotoLargeUrl:        this.fotoLargeUrl,
                fotoMediumUrl:          this.fotoMediumUrl,
                fotoThumbnailUrl:       this.fotoThumbnailUrl
            }
        }//

        getRecordTableProfessions() {
            const t: ITableProfessions[] = [];
            this.tableProfessions.forEach((el, i)=> {
                t.push({ worker: this.id, rowNumber: i, profession: el.id });
            });
            return t;
        }//
        
    }//

    export class List {
    
        public list: Ref[] = [];

        constructor ( recordList?: IRef[] ) {
            if (!recordList) return;
            for (let rec of recordList) { this.list.push( new Ref(rec) ) }
        }//

        refresh(dataServise: DataService): Observable<any> {

            let o$: Observable<any> = EMPTY;
            this.list.forEach(el => { 
                o$ = concat(o$, el.refresh(dataServise));
            });
            return o$.pipe(toArray());
        }//

    }//



}//