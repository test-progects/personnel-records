import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListWorkersContainer } from "./list/list-workers.container";
import { RefWorkersContainer } from "./ref/ref-workers.container";
import { workersObjects } from "./models/workers.model";


const routes: Routes = [
  
  { path: 'list-workers',//workersObjects.urlList,  
    pathMatch: 'full', 
    component: ListWorkersContainer 
  },
  { path: 'ref-workers/:id',//workersObjects.urlRef,  
    pathMatch: 'full', 
    component: RefWorkersContainer 
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class WorkersRoutingModule { }
