import { dbRef } from "@objects/models/object.model";
import { workersObjects } from "../models/workers.model";
import { EVariantSearch } from "@services/data.service";


export namespace actionsWorkers {

    export class GetList {
        static readonly type = '[Workers] GetList';
        constructor( public variantSearch: EVariantSearch, public filterStr: string ) {}
    }//

    export class GetBrothers {
        static readonly type = '[Workers] GetBrothers';
        constructor(public id:dbRef) {}
    }//

    export class GetItem {
        static readonly type = '[Workers] GetItem';
        constructor(public id:dbRef) {}
    }//

    export class NewItem {
        static readonly type = '[Workers] NewItem';
        constructor() {}
    }//

    export class SaveItem {
        static readonly type = '[Workers] SaveItem';
        constructor(public obj:workersObjects.IRef, public tableProfessions:workersObjects.ITableProfessions[]) {}
    }//

    //
    export class DeleteItem {
        static readonly type = '[Workers] DeleteItem';
        constructor(public id:dbRef) {}
    }//

}//

