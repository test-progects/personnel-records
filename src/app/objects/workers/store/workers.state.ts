import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector, createSelector } from '@ngxs/store';
import { tap } from 'rxjs/operators';

import { dbRef } from "@objects/models/object.model";
import { workersObjects } from "../models/workers.model";
import { DataService } from "@services/data.service";
import { actionsWorkers as Actions} from "./workers.actions";


interface IModelListState {
  list: workersObjects.IRef[];
  field1: string;
}//

@State<IModelListState>({
  name: 'workersList',
  defaults: {
    list: [],
    field1: 'test 0'
  }
})
@Injectable()
export class WorkersListState {

  constructor( private _dataService: DataService ) { }

  @Action(Actions.GetList)
  actionsGetList(ctx: StateContext<IModelListState>, {variantSearch, filterStr} : Actions.GetList) {
    return this._dataService.getWorkersList(variantSearch, filterStr).pipe(
      tap( (list) => { 
        ctx.patchState( {list: list} );
      })
    );
  }//


  @Selector()
  static selectorGetList(state: IModelListState) {
    return state.list;
  }//


}//


interface IModelItemState {
  item: workersObjects.IRef;
}//

@State<IModelItemState>({
  name: 'workersItem',
  defaults: {
    item: undefined
  }
})
@Injectable()
export class WorkersItemState {

  constructor( private _dataService: DataService ) { }

  @Action(Actions.GetItem)
  actionsGetItem(ctx: StateContext<IModelItemState>, {id}: Actions.GetItem) {
    if (!id) return undefined;
    return this._dataService.getWorkersItem(id).subscribe(
      (val)=>{ 
        if (val) ctx.patchState( {item: val} );
      }
    );
  }//

  static selectorGetItem(id:dbRef) {
    return createSelector([WorkersItemState], (state: IModelItemState) => {
      //return (state.item.id === id) ? new workersObjects.Ref( state.item ) : undefined;
      return (state.item.id === id) ? state.item : undefined;
      });
  }//

  @Action(Actions.NewItem)
  actionsNewItem(ctx: StateContext<IModelItemState>) {
  }//

  @Action(Actions.SaveItem)
  actionsSaveItem(ctx: StateContext<IModelItemState>, {obj, tableProfessions}: Actions.SaveItem) {
    this._dataService.saveWorkersItem(obj).subscribe();
    this._dataService.saveTableProfessionsForWorkers(obj.id, tableProfessions).subscribe();
  }//

  @Action(Actions.DeleteItem)
  actionsDeleteItem(ctx: StateContext<IModelItemState>, {id}: Actions.DeleteItem) {
    if (!id) return undefined;
    return this._dataService.deleteWorkersItem(id);
  }//


}//


interface IModelBrothersState {
  list: workersObjects.IRef[]; 
}//

@State<IModelBrothersState>({
  name: 'brothers',
  defaults: {
    list: []
  }
})
@Injectable()
export class BrothersState {

  constructor( private _dataService: DataService ) { }

  @Action(Actions.GetBrothers)
  actionsGetBrothers(ctx: StateContext<IModelBrothersState>, {id}:Actions.GetBrothers) {
    return this._dataService.getTableBrotherOfMind(id).pipe(
      tap( (list) => { 
        ctx.patchState( {list: list} );
      })
    );
  }//

  @Selector()
  static selectorGetBrothers(state: IModelBrothersState) {
    return new workersObjects.List(state.list);
  }//

}//