import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector, createSelector } from '@ngxs/store';
import { tap } from 'rxjs/operators';

import { dbRef } from "@objects/models/object.model";
import { workersObjects } from "../models/workers.model";
import { DataService } from "@services/data.service";
import { actionsWorkers as Actions} from "./workers.actions";

import { ObjectActions } from "@objects/store/object.actions";
import * as ObjStore from "@objects/store/object.state";


export const workersActions2 = new ObjectActions('workers');

interface IModelListState {
  list: workersObjects.IRef[];
}//

@State<IModelListState>({
  name: 'workersList2',
  defaults: {
    list: []
  }
})
@Injectable()
export class WorkersListState2 extends ObjStore.ObjectState {

  

  constructor( 
    private _dataService: DataService,

    ) {
    super( workersActions2, { getList: _dataService.getWorkersList } );
  }//

  
}//

