import { Component, Injector } from '@angular/core';

import { workersObjects } from "../models/workers.model";
import { WorkersListState } from '../store/workers.state';
import { actionsWorkers as Actions } from "../store/workers.actions";

import { Observable } from 'rxjs';
import { ListContainer } from "@shared/list.container";
import { delayWhen, map } from 'rxjs/operators';
import { filter } from 'rxjs/internal/operators';

import { EVariantSearch } from "@services/data.service";

import { MenuController } from '@ionic/angular';


@Component({
  templateUrl: './list-workers.container.html',
  styleUrls: ['./list-workers.container.scss']
})
export class ListWorkersContainer extends ListContainer {

  public object$: Observable<workersObjects.List>;
  public filterStr: string = "";
  public variantSearch = "All";

  constructor( protected readonly injector: Injector){
    super(injector);

    this.environs = {
      State : WorkersListState,
      actions: { 
        GetList: Actions.GetList
      },
      model: workersObjects,
      selectors: { GetList: WorkersListState.selectorGetList }
    };
  }//

  loadDate() {

    this.store.dispatch(new Actions.GetList( this.variantSearch as EVariantSearch, this.filterStr ));

    this.object$ = this.store.select(WorkersListState.selectorGetList)
    .pipe(
      filter(val=>!!val),
      map(val=>{ return new workersObjects.List(val as workersObjects.IRef[]) }),
      delayWhen(val=>val && val.refresh(this.dataService))
    );
  }//


  searchRefresh() {
    this.store.dispatch(new Actions.GetList( this.variantSearch as EVariantSearch, this.filterStr ));
  }//

  onCangeSearch(event) {
    this.searchRefresh();
  }//

  onChangeVariantSearch() {
    this.searchRefresh();
  }//

  onNew() {
    this.router.navigate(['/' + workersObjects.urlRef, ""]);
  }//

  openCustomMenu() {
    this.customMenu("list-workers-menu");
  }//
}//
