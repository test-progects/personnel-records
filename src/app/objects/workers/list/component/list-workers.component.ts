import { Component, Input, Output, EventEmitter } from '@angular/core';
import { workersObjects } from "../../models/workers.model";
import { ListComponent } from "@shared/list.component";

@Component({
  selector: 'app-list-workers',
  templateUrl: './list-workers.component.html',
  styleUrls: ['./list-workers.component.scss']
})
export class ListWorkersComponent extends ListComponent {

  @Input() list: workersObjects.Ref[];
  @Output() selectRow = new EventEmitter<workersObjects.Ref>();

  constructor() { 
    super();
  }//

}//
