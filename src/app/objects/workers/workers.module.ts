import { NgModule } from '@angular/core';
import { SharedModule } from "@app/modules/shared.module";
//
import { ListWorkersContainer } from "./list/list-workers.container";
import { ListWorkersComponent } from "./list/component/list-workers.component";
import { RefWorkersContainer } from "./ref/ref-workers.container";
import { RefWorkersComponent } from "./ref/component/ref-workers.component";
import { WorkersRoutingModule } from "./workers.routing";

@NgModule({

  imports: [
    SharedModule,
    WorkersRoutingModule,
  ],

  declarations: [
    ListWorkersContainer,
    ListWorkersComponent,
    RefWorkersContainer,
    RefWorkersComponent,

  ],

  exports: [
    WorkersRoutingModule,
    //
    ListWorkersContainer,
    ListWorkersComponent,
    RefWorkersContainer,
    RefWorkersComponent,

  ]

})
export class WorkersModule {}
