import { Component, Input, Injector } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { workersObjects } from "../../models/workers.model";
import { extractDate } from "@shared/other.util";

import { RefComponent } from "@shared/ref.component";

@Component({
  selector: 'app-ref-workers',
  templateUrl: './ref-workers.component.html',
  styleUrls: ['./ref-workers.component.scss']
})
export class RefWorkersComponent extends RefComponent {

  @Input() object: workersObjects.Ref;
  @Input() tableColleagues: workersObjects.Ref[] = [];

  form: FormGroup;

  formOptions = { 
    tableProfessionsOpen: false,
    tableColleaguesOpen: false,
    formModified: false
  }

  constructor( 
    protected readonly injector: Injector,
    protected readonly formBuilder: FormBuilder 
    ){
    super(injector);

    this.form = formBuilder.group({
      code:               ['', [Validators.required]],
      firstName:          ['', [Validators.required]],
      lastName:           ['', [Validators.required]],
      dateOfBirth:        ['', []]
    });
  }//

  formToObject() {
    super.formToObject();
    
    const v = this.form.value;
    this.object.code          = v.code;
    this.object.firstName     = v.firstName;
    this.object.lastName      = v.lastName;
    this.object.dateOfBirth   = extractDate(v.dateOfBirth);
  }//

  objectToForm() {

    if (!this.object) return;

    this.object.share.formOptions = this.formOptions;

    const data = {
      code:         this.object.code,
      firstName:    this.object.firstName,
      lastName:     this.object.lastName,
      dateOfBirth:  this.object && this.object.dateOfBirth && this.object.dateOfBirth.toString()
    };
    this.form.patchValue(data);
  }//

  ngOnInit() {}//

  onAddProfession() {
    this.emitEventForm("onAddProfession");
  }//

  delProfession(row) {
    this.emitEventForm("onDelProfession", row);
  }//

  swipeProfession() {}

  onClickProfession(val) {
    this.formOptions.tableProfessionsOpen = val;
  }//

  swipeColleagues() {}

  onClickColleagues(val) {
    this.formOptions.tableColleaguesOpen = val;
  }//

  openCustomMenu() { 
    this.customMenu("ref-workers-menu");
  }// 


}//
