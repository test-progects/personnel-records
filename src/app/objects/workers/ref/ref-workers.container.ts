import { Component, OnInit, Injector } from '@angular/core';
import { Observable, from } from 'rxjs';
import { delayWhen, tap, map } from 'rxjs/operators';
import { filter } from 'rxjs/internal/operators';
//
import { workersObjects } from "../models/workers.model";
import { WorkersItemState } from '../store/workers.state';
import { actionsWorkers as Actions } from "../store/workers.actions";
import { dbRef } from "@objects/models/object.model";

import { RefContainer } from "@shared/ref.container";

import { ModalController } from '@ionic/angular';
import { SelectProfessionsContainer } from "@objects/professions/select/select-professions.container";


@Component({
  templateUrl: './ref-workers.container.html',
  styleUrls: ['./ref-workers.container.scss']
})
export class RefWorkersContainer extends RefContainer implements OnInit {

  public object$: Observable<workersObjects.Ref>;
  public object: workersObjects.Ref = new workersObjects.Ref();
  public tableColleagues: workersObjects.Ref[] = [];


  constructor( 
    protected readonly injector: Injector,
    public modalController: ModalController 
    ){
      super(injector);

      this.environs = {
        State : WorkersItemState,
        actions: { 
          GetItem: Actions.GetItem,
          DeleteItem: Actions.DeleteItem,
          NewItem: Actions.NewItem,
          SaveItem: Actions.SaveItem
        },
        model: workersObjects,
        selectors: { GetItem: WorkersItemState.selectorGetItem }
      };

    }//

  ngOnInit() {

    // from([1,2,3]).pipe(
    //   filter(el=>{return el === 2 }),
    //   tap(el=>{console.log('tap ', el) })
    // ).toPromise().then(el=>{ console.log('then', el)}).catch(el=>{console.log('catch', el) });

  }//

  loadData() {

    const id = this._id as dbRef;
    this.store.dispatch(new Actions.GetItem(id)).toPromise().then(_=>{
      
      this.store.selectOnce(this.environs.State.selectorGetItem(id))
      .pipe(
        filter(val=>!!val),
        map(val=>{ return new workersObjects.Ref(val as workersObjects.IRef) }),
        delayWhen(val=>{
          return (val as workersObjects.Ref).refresh(this.dataService)})
      ).subscribe(val=>{ 
        this.object = val as workersObjects.Ref;
        this.refreshCollaegues();      
      });
    });

  }//

  refreshCollaegues(){
    if (this.object.newItem) return;

    const tableColleagues$=this.object.getColleagues(this.dataService);
    tableColleagues$.subscribe(val=>{this.tableColleagues = val.list});
  }//

  eventForm(ctx) {
    super.eventForm(ctx);

    switch (ctx.name) {
      case "onAddProfession": 
      this.presentSelectProfession();
      break;

      case "onDelProfession": 
      this.onDelProfession(ctx.value);
      break;

      default: break;
    }
  }//
  
  onDelProfession(row) {
    if (!row) return;
    this.object.tableProfessions = this.object.tableProfessions.filter(el=>{ return ( row.id !== el.id )});
    //
    this.object.share.formOptions.formModified = true;
  }//

  async presentSelectProfession() {
    const modal = await this.modalController.create({
      component: SelectProfessionsContainer,
      cssClass: 'my-custom-class'
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    this.afterSelectProfession(data);
  }//

  afterSelectProfession(sel) {
    if (!(sel && sel.profession)) return;
    if (this.object.tableProfessions.find(el=>{return el.id === sel.profession.id})) return;
    this.object.tableProfessions.push(sel.profession);
    //
    this.object.share.formOptions.formModified = true;
  }//

  onSave() {
    const record = this.object.getRecordDB();
    const recordProfessions = this.object.getRecordTableProfessions();
    
    this.store.dispatch(new this.environs.actions.SaveItem(record, recordProfessions));
    this.router.navigate(['/' + this.environs.model.urlList]);
  }//

}//
