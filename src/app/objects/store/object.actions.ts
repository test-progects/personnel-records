import { dbRef } from "@objects/models/object.model";

export class ObjectActions {
    
    public GetList;
    public GetItem;
    public NewItem;
    public SaveItem;

    constructor (private _objectName = "") {

        this.GetList = class {
            static readonly type = '[' + _objectName + '] GetList';
            constructor() {}
        };

        this.GetItem = class {
            static readonly type = '[' + _objectName + '] GetItem';
            constructor(public id: dbRef) {}
        };

        this.NewItem = class {
            static readonly type = '[' + _objectName + '] NewItem';
            constructor() {}
        };

        this.SaveItem = class <T> {
            static readonly type = '[' + _objectName + '] SaveItem';
            constructor(public obj: T) {}
        };
    }//
}//

