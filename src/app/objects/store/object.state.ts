import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector, createSelector } from '@ngxs/store';
import { tap } from 'rxjs/operators';

import { dbRef } from "@objects/models/object.model";

export interface IActions {
  GetLList?;
  GetItem?;
  NewItem?;
  SaveItem?
}//

export interface IDataMetods {
  getList;
}

interface IModelState {
  list: any;
  item: any;
}//


export class GetList2 {
  static readonly type = '[Workers] GetList';
  constructor() {}
}//


@State<IModelState>({
  name: 'objectState',
  defaults: {
    list: [],
    item: undefined
  }
})
@Injectable()
export class ObjectState {

  constructor(
    private _actions: IActions, 
    private _dataMetods: IDataMetods
  ) {}

  // @Action(this._actions.GetList)
  // actionsGetList(ctx: StateContext<IModelState>) {
    
  //   return this._dataMetods.getList().pipe(
  //     tap( (list) => { 
  //       ctx.patchState( {list: list} );
  //     })
  //   );
  // }//


  @Action(GetList2)
  actionsGetList(ctx: StateContext<IModelState>) {
    
    return this._dataMetods.getList().pipe(
      tap( (list) => { 
        ctx.patchState( {list: list} );
      })
    );
  }//

  @Action(GetList2) f1 =
  function(ctx: StateContext<IModelState>) {
    
    return this._dataMetods.getList().pipe(
      tap( (list) => { 
        ctx.patchState( {list: list} );
      })
    );
  }//


  static selectorGetList( stateClass, objectClass ) {
    return createSelector([stateClass], (state: { list: any[] }) => {
      return new objectClass(state.list);
    });
  }//

  
  

}//

