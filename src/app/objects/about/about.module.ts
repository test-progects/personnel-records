import { NgModule } from '@angular/core';
import { SharedModule } from "@app/modules/shared.module";
//
import { AboutComponent } from "./component/about.component";
import { AboutRoutingModule } from "./about.routing";

@NgModule({

  imports: [
    SharedModule,
    AboutRoutingModule,
  ],

  declarations: [
    AboutComponent
  ],

  exports: [
    AboutRoutingModule,
    AboutComponent
  ]

})
export class AboutModule {}
