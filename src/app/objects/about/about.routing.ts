import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from "./component/about.component";


const routes: Routes = [
  
  { path: 'about',  
    pathMatch: 'full', 
    component: AboutComponent 
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AboutRoutingModule { }
