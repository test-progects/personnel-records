
import * as objectModel from "@objects/models/object.model";
import { getNewReference } from "@shared/dbRef.util";
import { fillProp } from "@shared/other.util";
import { Observable, of, EMPTY, concat } from 'rxjs';
import { toArray, map, delayWhen, tap } from 'rxjs/operators';
import { DataService } from "@services/data.service";
import { dutiesObjects } from "@objects/duties/models/duties.model";


export namespace professionsObjects {

    export interface IRef {
        id:         objectModel.dbRef;
        name:       string;
        sum:        number;
    }//

    export interface IDataHTTP {
        list:       IRef[];
    }//

    export interface ITableDuties {
        profession: objectModel.dbRef;
        rowNumber:  number;
        duty:       objectModel.dbRef;
    }//

    export const prefixDbRef    = 'professions';
    export const urlRef         = 'ref-professions';
    export const urlList        = 'list-professions';

    export class Ref extends objectModel.MetadataObject implements IRef  {

        id:   objectModel.dbRef = null;
        name: string            = null;
        sum:  number            = null;
        //
        tableDuties: dutiesObjects.Ref[] = [];
        //
        share: Record<string, any> = {};
        newItem = false;

        constructor ( record?: IRef ) {

            super();
            if (!record) {
                //Это новый объект
                this.newItem = true; 
                this.id = getNewReference(prefixDbRef);
                return;
            };

            fillProp(this, record);
        }//

        refresh(dataServise: DataService): Observable<any> {
            
            if (!this.id || this.newItem) return of(0);

            //duties
            let duty$ = dataServise.getTableDutiesForProfessions(this.id).pipe(
                map(duties=>{ return new dutiesObjects.List(duties) }),
                delayWhen(list=>list.refresh(dataServise)),
                tap(list=>{ 
                    this.tableDuties = list.list;
                    //
                })
            );

            //+ еще операции

            let o$: Observable<any> = EMPTY;
            o$ = concat(o$, duty$);
            return o$.pipe(toArray());
        }//

        getRecordDB() {
            return {
                id:             this.id,
                name:           this.name,
                sum:            this.sum
            }
        }//

        getRecordTableDuties() {
            const t: ITableDuties[] = [];
            this.tableDuties.forEach((el, i)=> {
                t.push({ profession: this.id, rowNumber: i, duty: el.id });
            });
            return t;
        }//
        
    }//

    export class List {
    
        public list: Ref[] = [];

        constructor ( recordList?: IRef[] ) {
            if (!recordList) return;
            for (let rec of recordList) { this.list.push( new Ref(rec) ) }
        }//

        refresh(dataServise: DataService): Observable<any> {

            let o$: Observable<any> = EMPTY;
            this.list.forEach(el => { 
                o$ = concat(o$, el.refresh(dataServise));
            });
            return o$.pipe(toArray());
        }//

    }//



}//