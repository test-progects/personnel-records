import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, Injector } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { professionsObjects } from "../../models/professions.model";
import { RefComponent } from "@shared/ref.component";

@Component({
  selector: 'app-ref-professions',
  templateUrl: './ref-professions.component.html',
  styleUrls: ['./ref-professions.component.scss']
})
export class RefProfessionsComponent extends RefComponent {

  @Input() object: professionsObjects.Ref;

  form: FormGroup;

  formOptions = { 
    tableDutiesOpen: false,
    formModified: false
  }

  constructor( 
    protected readonly injector: Injector,
    protected readonly formBuilder: FormBuilder
    ){
    super(injector);

    this.form = formBuilder.group({
      name:          ['', [Validators.required]],
      sum:           ['', []]
    });
    
  }//

  formToObject() {
    super.formToObject();
    
    const v = this.form.value;
    this.object.name    = v.name;
    this.object.sum     = v.sum;
  }//

  objectToForm() {
    this.object.share.formOptions = this.formOptions;

    const data = {
      name:         this.object.name,
      sum:          this.object.sum
    };
    this.form.patchValue(data);
  }//
 
  ngOnInit() {   
  }//

  onDelDuty(row) {
    this.emitEventForm("onDelDuty", row);
  }//

  onAddDuty() {
    this.emitEventForm("onAddDuty");
  }//

  swipeDuty() {}

  onClickDuties(val) {
    this.formOptions.tableDutiesOpen = val;
  }//

  openCustomMenu() {
    this.customMenu("ref-professions-menu");
  }//

}//
