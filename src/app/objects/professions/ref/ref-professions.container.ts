import { Component, OnInit, Injector } from '@angular/core';
import { Observable } from 'rxjs';
//
import { professionsObjects } from "../models/professions.model";
import { ProfessionsItemState } from '../store/professions.state';
import { actionsProfessions as Actions } from "../store/professions.actions";
//
import { RefContainer } from "@shared/ref.container";

import { ModalController } from '@ionic/angular';
import { SelectDutiesContainer } from "@objects/duties/select/select-duties.container";


@Component({
  templateUrl: './ref-professions.container.html',
  styleUrls: ['./ref-professions.container.scss']
})
export class RefProfessionsContainer extends RefContainer implements OnInit {

  public object$: Observable<professionsObjects.Ref>;
  public object: professionsObjects.Ref;

  constructor( 
    protected readonly injector: Injector, 
    public modalController: ModalController 
    ){
    super(injector);

    this.environs = {
      State : ProfessionsItemState,
      actions: { 
        GetItem: Actions.GetItem,
        DeleteItem: Actions.DeleteItem,
        NewItem: Actions.NewItem,
        SaveItem: Actions.SaveItem
      },
      model: professionsObjects,
      selectors: { GetItem: ProfessionsItemState.selectorGetItem }
    };
  }//

  ngOnInit() {}

  eventForm(ctx) {
    super.eventForm(ctx);

    switch (ctx.name) {
      case "onAddDuty": 
      this.presentSelectDuty();
      break;

      case "onDelDuty": 
      this.onDelDuty(ctx.value);
      break;

      default: break;
    }
  }//

  onDelDuty(row) {
    if (!row) return;
    this.object.tableDuties = this.object.tableDuties.filter(el=>{ return ( row.id !== el.id )});
    //
    this.object.share.formOptions.formModified = true;
  }//

  async presentSelectDuty() {
    const modal = await this.modalController.create({
      component: SelectDutiesContainer,
      cssClass: 'my-custom-class'
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    this.afterSelectDuty(data);
  }//

  afterSelectDuty(sel) {
    if (!(sel && sel.duty)) return;
    if (this.object.tableDuties.find(el=>{return el.id === sel.duty.id})) return;
    this.object.tableDuties.push(sel.duty);
    this.object.share.formOptions.formModified = true;
  }//

  onSave() {
    const record = this.object.getRecordDB();
    const recordDuties = this.object.getRecordTableDuties();
    
    this.store.dispatch(new this.environs.actions.SaveItem(record, recordDuties));
    this.router.navigate(['/' + this.environs.model.urlList]);
  }//

}//
