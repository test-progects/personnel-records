import { Component, Input, Output, EventEmitter } from '@angular/core';
import { professionsObjects } from "../../models/professions.model";
import { ListComponent } from "@shared/list.component";

@Component({
  selector: 'app-list-professions',
  templateUrl: './list-professions.component.html',
  styleUrls: ['./list-professions.component.scss']
})
export class ListProfessionsComponent extends ListComponent {

  @Input() list: professionsObjects.Ref[];
  @Output() selectRow = new EventEmitter<professionsObjects.Ref>();

  constructor() { 
    super();
  }//

}//
