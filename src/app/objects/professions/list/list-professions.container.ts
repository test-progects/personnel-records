import { Component, OnInit, Injector } from '@angular/core';

import { professionsObjects } from "../models/professions.model";
import { ProfessionsListState } from '../store/professions.state';
import { actionsProfessions as Actions } from "../store/professions.actions";

import { Observable } from 'rxjs';
import { ListContainer } from "@shared/list.container";

@Component({
  templateUrl: './list-professions.container.html',
  styleUrls: ['./list-professions.container.scss']
})
export class ListProfessionsContainer extends ListContainer implements OnInit {

  public object$: Observable<professionsObjects.List>;

  constructor( protected readonly injector: Injector){
    super(injector);

    this.environs = {
      State : ProfessionsListState,
      actions: { 
        GetList: Actions.GetList
      },
      model: professionsObjects,
      selectors: { GetList: ProfessionsListState.selectorGetList }
    };
  }//

  ngOnInit() {
  }//

  onNew() {
    this.router.navigate(['/' + professionsObjects.urlRef, ""]);
  }//

  openCustomMenu() {
    this.customMenu("list-professions-menu");
  }//

}//
