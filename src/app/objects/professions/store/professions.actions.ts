import { dbRef } from "@objects/models/object.model";
import { professionsObjects } from "../models/professions.model";


export namespace actionsProfessions {

    export class GetList {
        static readonly type = '[Professions] GetList';
        constructor() {}
    }//

    export class GetItem {
        static readonly type = '[Professions] GetItem';
        constructor(public id:dbRef) {}
    }//

    export class NewItem {
        static readonly type = '[Professions] NewItem';
        constructor() {}
    }//

    export class SaveItem {
        static readonly type = '[Professions] SaveItem';
        constructor(public obj:professionsObjects.IRef, public tableDuties:professionsObjects.ITableDuties[]) {}
    }//

    //
    export class DeleteItem {
        static readonly type = '[Professions] DeleteItem';
        constructor(public id:dbRef) {}
    }//

}//

