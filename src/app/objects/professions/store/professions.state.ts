import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector, createSelector } from '@ngxs/store';
import { tap } from 'rxjs/operators';

import { dbRef } from "@objects/models/object.model";
import { professionsObjects } from "../models/professions.model";
import { DataService } from "@services/data.service";
import { actionsProfessions as Actions} from "./professions.actions";


interface IModelListState {
  list: professionsObjects.IRef[];
}//

@State<IModelListState>({
  name: 'professionsList',
  defaults: {
    list: []
  }
})
@Injectable()
export class ProfessionsListState {

  constructor( private _dataService: DataService ) { }

  @Action(Actions.GetList)
  actionsGetList(ctx: StateContext<IModelListState>) {
    return this._dataService.getProfessionsList().pipe(
      tap( (list) => { 
        ctx.patchState( {list: list} );
      })
    );
  }//

  @Selector()
  static selectorGetList(state: IModelListState) {
    return state.list;
  }//

}//


interface IModelItemState {
  item: professionsObjects.IRef;
}//

@State<IModelItemState>({
  name: 'professionsItem',
  defaults: {
    item: undefined
  }
})
@Injectable()
export class ProfessionsItemState {

  constructor( private _dataService: DataService ) { }

  @Action(Actions.GetItem)
  actionsGetItem(ctx: StateContext<IModelItemState>, {id}: Actions.GetItem) {
    if (!id) return undefined;
    return this._dataService.getProfessionsItem(id).subscribe(
      (val)=>{ 
        if (val) ctx.patchState( {item: val} );
      }
    );
  }//

  static selectorGetItem(id:dbRef) {
    return createSelector([ProfessionsItemState], (state: IModelItemState) => {
      //return (state.item.id === id) ? new professionsObjects.Ref(state.item) : undefined;
      return (state.item.id === id) ? state.item : undefined;
      });
  }//

  @Action(Actions.NewItem)
  actionsNewItem(ctx: StateContext<IModelItemState>) {
  }//

  @Action(Actions.SaveItem)
  actionsSaveItem(ctx: StateContext<IModelItemState>, {obj, tableDuties}: Actions.SaveItem) {
    this._dataService.saveProfessionsItem(obj).subscribe();
    this._dataService.saveTableDutiesForProfessions(obj.id, tableDuties).subscribe();
  }//

  @Action(Actions.DeleteItem)
  actionsDeleteItem(ctx: StateContext<IModelItemState>, {id}: Actions.DeleteItem) {
    if (!id) return undefined;
    return this._dataService.deleteProfessionsItem(id);
  }//


}//

