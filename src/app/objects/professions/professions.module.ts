import { NgModule } from '@angular/core';
import { SharedModule } from "@app/modules/shared.module";
//
import { ListProfessionsContainer } from "./list/list-professions.container";
import { ListProfessionsComponent } from "./list/component/list-professions.component";
import { RefProfessionsContainer } from "./ref/ref-professions.container";
import { RefProfessionsComponent } from "./ref/component/ref-professions.component";
import { ProfessionsRoutingModule } from "./professions.routing";
import { SelectProfessionsContainer } from "./select/select-professions.container";
import { SelectProfessionsComponent } from "./select/component/select-professions.component";

@NgModule({

  imports: [
    SharedModule,
    ProfessionsRoutingModule,
  ],

  declarations: [
    ListProfessionsContainer,
    ListProfessionsComponent,
    RefProfessionsContainer,
    RefProfessionsComponent,

    SelectProfessionsContainer,
    SelectProfessionsComponent

  ],

  exports: [
    ProfessionsRoutingModule,
    //
    ListProfessionsContainer,
    ListProfessionsComponent,
    RefProfessionsContainer, 
    RefProfessionsComponent,

    SelectProfessionsContainer

  ],

  entryComponents: [
  SelectProfessionsContainer,
  SelectProfessionsComponent
  ]

})
export class ProfessionsModule {}
