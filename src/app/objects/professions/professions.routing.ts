import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListProfessionsContainer } from "./list/list-professions.container";
import { RefProfessionsContainer } from "./ref/ref-professions.container";
import { professionsObjects } from "./models/professions.model";


const routes: Routes = [
  
  { path: 'list-professions',//workersObjects.urlList,  
    pathMatch: 'full', 
    component: ListProfessionsContainer 
  },
  { path: 'ref-professions/:id',//workersObjects.urlRef,  
    pathMatch: 'full', 
    component: RefProfessionsContainer 
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class ProfessionsRoutingModule { }
