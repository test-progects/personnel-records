import { Component, Input, Output, EventEmitter } from '@angular/core';
import { professionsObjects } from "../../models/professions.model";
import { ListComponent } from "@shared/list.component";

@Component({
  selector: 'app-select-professions',
  templateUrl: './select-professions.component.html',
  styleUrls: ['./select-professions.component.scss']
})
export class SelectProfessionsComponent extends ListComponent {

  @Input() list: professionsObjects.Ref[];
  @Output() selectRow = new EventEmitter<professionsObjects.Ref>();

  constructor() {
    super();
  }//

  selectItem(row) {
    this.selectRow.emit(row);
  }//

}//
