import { Component, OnInit, Injector } from '@angular/core';

import { professionsObjects } from "../models/professions.model";
import { ProfessionsListState } from '../store/professions.state';
import { actionsProfessions as Actions } from "../store/professions.actions";

import { Observable } from 'rxjs';
import { ListContainer } from "@shared/list.container";
import { ModalController } from '@ionic/angular';

@Component({
  templateUrl: './select-professions.container.html',
  styleUrls: ['./select-professions.container.scss']
})
export class SelectProfessionsContainer extends ListContainer implements OnInit {

  public object$: Observable<professionsObjects.List>;

  constructor( protected readonly injector: Injector,
    public modalController: ModalController
    ){
    super(injector);

    this.environs = {
      State : ProfessionsListState,
      actions: { 
        GetList: Actions.GetList
      },
      model: professionsObjects,
      selectors: { GetList: ProfessionsListState.selectorGetList }
    };

  }//

  ngOnInit() {
    this.loadDate();
  }//

  selectRow(row) {
    this.modalController.dismiss({'profession': row });
  }//

  onClose() {
    this.modalController.dismiss();
  }//

}//
