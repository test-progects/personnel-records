import { NgModule } from '@angular/core';
import { SharedModule } from "./shared.module";
import { WorkersModule } from "@objects/workers/workers.module";
import { ProfessionsModule } from "@objects/professions/professions.module";
import { DutiesModule } from "@objects/duties/duties.module";
import { AboutModule } from "@objects/about/about.module";
//
import { BrothersState, WorkersItemState, WorkersListState } from "@objects/workers/store/workers.state";
import { ProfessionsItemState, ProfessionsListState } from "@objects/professions/store/professions.state";
import { DutiesItemState, DutiesListState } from "@objects/duties/store/duties.state";


import { environment } from "@app/../environments/environment";
import { NgxsModule } from '@ngxs/store';


@NgModule({

  imports: [
    SharedModule,
    WorkersModule,
    ProfessionsModule,
    DutiesModule,
    AboutModule,
    //
    NgxsModule.forRoot(
      [
        BrothersState, 
        WorkersItemState, 
        WorkersListState,
        ProfessionsItemState, 
        ProfessionsListState,
        DutiesItemState,
        DutiesListState
      ], { developmentMode: !environment.production }),
  ],

  declarations: [],

  exports: []
})
export class ProgectModule {}
