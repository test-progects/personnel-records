import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from '../app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import { DatePickerModule } from "../modules/date-picker.module";
//import { MatNativeDateModule } from '@angular/material/core';
//import { MaterialModule } from "../modules/material.module";
import { ReactiveFormsModule, FormsModule } from '@angular/forms'

import { IonicModule } from '@ionic/angular';


@NgModule({

  exports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FormsModule,

    IonicModule,
    //
    //MaterialModule,
    //MatNativeDateModule,
    //DatePickerModule,
  ],

})
export class SharedModule { }
