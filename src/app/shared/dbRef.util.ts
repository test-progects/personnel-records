import { v4 as uuid4 } from "uuid";
import * as objectModel from "@objects/models/object.model";

//simple=94df9991-b975-488b-a0b7-a709fb0e30e4

export function getNewReference(prefix: string) {

    if (!prefix) {return ''}

    return prefix + "=" + uuid4();
}//

export function isReference(ref: objectModel.dbRef) {

    if (!ref) {return false}

    if (!ref.includes('=')) {return false}

    return true;
}//
