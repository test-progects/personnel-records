import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  templateUrl: './list.component.html'
})
export class ListComponent implements OnInit {

  @Input() list: any;
  @Output() selectRow = new EventEmitter<any>();

  constructor() {}

  ngOnInit() {
  }//

  edit(row) {
    this.selectRow.emit(row);
  }//

  swipe() {}

}//
