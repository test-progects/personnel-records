export function formatDate(date2) {
    if (!date2) return '';

    let date = new Date(Date.parse(date2));

    let dd; 
    dd = date.getDate();
    if (dd < 10) dd = '0' + dd;
  
    let mm;
    mm = date.getMonth() + 1;
    if (mm < 10) mm = '0' + mm;
  
    let yy; 
    yy = date.getFullYear() % 100;
    if (yy < 10) yy = '0' + yy;
  
    return dd + '.' + mm + '.' + yy;
}//
  
export function is(type: string, obj: any): boolean {
    let clas = Object.prototype.toString
      .call(obj)
      .slice(8, -1)
    return obj !== undefined && obj !== null && clas === type
}//

export function extractDate(v : string | Date) {

  if ( is('Date', v) ) { return v as Date; }

  if ( is('String', v) ) { return new Date( Date.parse( v as string ) ); }

  return new Date;

}//

export function fillProp(dest: Object, source: Object) {

  let keysSource = Object.keys(source);
  let keysDest = Object.keys(dest);
  
  for (let key of keysSource) { 
    if (keysDest.includes(key)) { dest[key] = source[key] }
  }  
}//

export let styleKit = {
  currentRow: { backgroundColor: "rgb(127, 255, 234)" },
  deletedDoc: { color: "red" },
  doneDoc:    { color: "green" }
};

export function getDocIcon( { deleteMark, doneMark } ) {
  if (deleteMark) return "bookmark_remove";
  if (doneMark) return "bookmark_added";
  return "bookmark_border";
}//

export function getDocIconStyle( { deleteMark, doneMark } ) {
  if (deleteMark) return styleKit.deletedDoc;
  if (doneMark) return styleKit.doneDoc;
  return "";
}//

export function getRefIcon( { deleteMark } ) {
  return (deleteMark) ? "highlight_off" : "panorama_fish_eye";
}//

export function getRefIconStyle( { deleteMark } ) {
  return (deleteMark) ? styleKit.deletedDoc: "";
}//
