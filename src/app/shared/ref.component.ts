import { Component, EventEmitter, Injector, Output, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';

import { MenuController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';

@Component({
  templateUrl: './ref.component.html'
})

export class RefComponent implements OnInit, OnChanges {

  @Input() object: any;
  @Output() eventForm = new EventEmitter<any>();

  formOptions = { 
    formModified: false
  }

  protected readonly menu: MenuController;
  protected readonly alertControl: AlertController;

  constructor( protected injector: Injector ) {

    this.menu = injector.get<MenuController>(MenuController);
    this.alertControl = injector.get<AlertController>(AlertController);
  }//

  ngOnInit() {   
  }//

  marckModified(){return (this.formOptions.formModified) ? "*" : ""}

  formToObject() {
    this.formOptions.formModified = true;
  }//

  objectToForm() {
  }//
 
  ngOnChanges(change: SimpleChanges) {
    this.objectToForm();
    this.formOptions.formModified = false;
  }//

  emitEventForm(name:string, value?, event?) {
    this.eventForm.emit({ name: name, value: value, event: event });
  }//

  onChangeField(event, name) {
    this.formToObject();
    //this.emitEventForm(name, this.form.value[name], event);
    this.emitEventForm(name, this.object[name], event);
  }//

  customMenu(menuId:string) {
    this.menu.enable(true, menuId);
    this.menu.open(menuId);
  }//

  onSave() {
    this.getConfirm('Save item?', "",()=>{this.saveThisItem()});
  }//

  saveThisItem() {
    this.formToObject();
    this.formOptions.formModified = false;
    this.emitEventForm("onSave", true);
  }//

  onDelete() {
    this.getConfirm('Delete item?', "",()=>{this.deleteThisItem()});
  }//

  deleteThisItem() {
    this.formToObject();
    this.emitEventForm("onDelete", true);
  }//
  
  async getConfirm(header:string="", message:string="", callback:()=>void) {

    const alert = await this.alertControl.create({
      header: header,
      message: message,
      buttons: [{text:'Yes', handler:callback}, 'No']
    });

    await alert.present();
  }//

}//
