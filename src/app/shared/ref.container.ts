import { Observable } from 'rxjs';
import { delayWhen, map } from 'rxjs/operators';
import { filter } from 'rxjs/internal/operators';
import { ActivatedRoute, Router } from '@angular/router';
//
import { Store } from '@ngxs/store';
import { DataService } from "@services/data.service";
import { dbRef } from "@objects/models/object.model";
import { Injector } from '@angular/core';


export interface IEnvirons {
  model: any;
  actions: {GetItem:any, NewItem:any, SaveItem:any, DeleteItem:any};
  selectors: {GetItem:any};
  State: any;
}

export class RefContainer {

  public object$: Observable<any>;
  public object: any;
  protected _id: string;

  public environs: IEnvirons = {
    model: null,
    actions: null,
    selectors: null,
    State: null
  }

  protected readonly store: Store;
  protected readonly dataService: DataService;
  protected readonly route: ActivatedRoute;
  protected readonly router: Router;

  constructor( protected injector: Injector ){
      this.store = injector.get<Store>(Store);
      this.dataService = injector.get<DataService>(DataService);
      this.route = injector.get<ActivatedRoute>(ActivatedRoute);
      this.router = injector.get<Router>(Router);

      this.route.params.subscribe((par) => {
        this._id = par['id'];
        this.loadData();
      });
  }//


  loadData() {
    const id = this._id as dbRef;
    this.store.dispatch(new this.environs.actions.GetItem(id)).toPromise().then(_=>{
      
      this.store.selectOnce(this.environs.selectors.GetItem(id))
      .pipe(
        filter(val=>!!val),
        map(val=>{ return new this.environs.model.Ref(val) }),
        delayWhen(val=>{return (val as {refresh:(v:DataService)=>any}).refresh(this.dataService)})
      ).subscribe(val=>{ 
        this.object = val;
      });
    });

    this.object = new this.environs.model.Ref();
  }//

  eventForm(ctx) {

    switch (ctx.name) {
      case "onSave": this.onSave();
        break;

      case "onDelete": this.onDelete();
        break;
    
      default:
        break;
    }
    
  }//
  
  onSave() {
    const record = this.object.getRecordDB();
    this.store.dispatch(new this.environs.actions.SaveItem(record));
    this.router.navigate(['/' + this.environs.model.urlList]);
  }//

  onDelete() {
    this.store.dispatch(new this.environs.actions.DeleteItem(this.object.id));
    this.router.navigate(['/' + this.environs.model.urlList]);
  }//

}//
