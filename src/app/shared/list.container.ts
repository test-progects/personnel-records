import { Observable } from 'rxjs';
import { delayWhen, map } from 'rxjs/operators';
import { filter } from 'rxjs/internal/operators';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
//
import { Store } from '@ngxs/store';
import { DataService } from "@services/data.service";
import { Injector } from '@angular/core';
import { MenuController } from '@ionic/angular';

export interface IEnvirons {
  model: any;
  actions: {GetList:any};
  selectors: {GetList:any};
  State: any;
}

export class ListContainer {

  public object$: Observable<any>;

  public environs: IEnvirons = {
    model: null,
    actions: null,
    selectors: null,
    State: null
  }

  protected readonly store: Store;
  protected readonly dataService: DataService;
  protected readonly route: ActivatedRoute;
  protected readonly router: Router;
  protected readonly menu: MenuController;

  constructor( protected injector: Injector ){
    this.store = injector.get<Store>(Store);
    this.dataService = injector.get<DataService>(DataService);
    this.route = injector.get<ActivatedRoute>(ActivatedRoute);
    this.router = injector.get<Router>(Router);
    this.menu = injector.get<MenuController>(MenuController);

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {this.loadDate()}
    });
  }//

  loadDate() {

    this.store.dispatch(new this.environs.actions.GetList());

    this.object$ = this.store.select(this.environs.selectors.GetList)
    .pipe(
      filter(val=>!!val),
      map(val=>{ return new this.environs.model.List(val) }),
      delayWhen(val=>val && val.refresh(this.dataService))
    );
  }//

  selectRow(row) {
    if (!row) return;
    this.router.navigate(['/' + this.environs.model.urlRef, row.id]);
  }//

  customMenu(menuId:string) {
    this.menu.enable(true, menuId);
    this.menu.open(menuId);
  }//

  onNew() {
  }//

}//
