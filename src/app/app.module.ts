import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { DataService } from "./services/data.service";
import { ProgectModule } from "./modules/progect.module";
import { SharedModule } from "./modules/shared.module";

import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';


@NgModule({
  declarations: [
    AppComponent,
  ],

  imports: [
    SharedModule,
    ProgectModule,
    IonicModule.forRoot(),
  ],
  providers: [
    DataService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }